package org.synergyftc.robot;

import android.app.AlertDialog;
import android.content.DialogInterface;

import com.qualcomm.robotcore.eventloop.opmode.OpMode;

import org.synergyftc.robot.motion.AddedMovements;
import org.synergyftc.robot.motion.CDistanceMove;
import org.synergyftc.robot.motion.ClimberDumpMovement;
import org.synergyftc.robot.motion.DistanceMove;
import org.synergyftc.robot.motion.GyroTurn;
import org.synergyftc.robot.motion.MiscMovement;
import org.synergyftc.robot.motion.Pause;
import org.synergyftc.robot.motion.SequentialMotionController;

/**
 * Created by Logan on 1/24/2016.
 */
public class Autonomous extends OpMode {
    Motors motors;
    Sensors sensors;
    SequentialMotionController motion;

    @Override
    public void init() {
        motors = new Motors(this);
        sensors = new Sensors(this);
        motion = new SequentialMotionController(motors, sensors);
        motors.left.resetEncoder();
        motors.right.resetEncoder();

//        AlertDialog.Builder adb = new AlertDialog.Builder(hardwareMap.appContext);
//        adb.setTitle("This is a test");
//        adb.setMessage("Click ok and keep on memeing.");
//        adb.setMultiChoiceItems(new String[]{
//                "Synergy",
//                "ROcks",
//                "Bro"
//        }, new boolean[]{false, false, false}, new DialogInterface.OnMultiChoiceClickListener() {
//            @Override
//            public void onClick(DialogInterface dialog, int which, boolean isChecked) {
//
//            }
//        });
//        AlertDialog dlg = adb.create();
//        dlg.show();

        motion.addMovement(
                new CDistanceMove(Distance.inches(0.1), 0.5), //// FIXME: 1/28/2016

                //sweep toward climber goal
                new AddedMovements(
                        new DistanceMove(Distance.inches(-4), 0.5),
                        new MiscMovement(-1, 0)
                ),
                new Pause(0.3),
                new GyroTurn(360-50, 0.4),
                new Pause(0.3),
                new AddedMovements(
                        new DistanceMove(Distance.inches(-88), 0.4),
                        new MiscMovement(-1, 0)
                ),
                new Pause(0.3),

                //the big dump
                new ClimberDumpMovement(),
                new Pause(0.3),

                //align with mountain
                new CDistanceMove(Distance.inches(23), 0.4),
                new Pause(0.3),
                new GyroTurn(52, 0.4),
                new Pause(0.3),

                //mountain climb
                new CDistanceMove(Distance.inches(36), 0.9),
                new AddedMovements(
                        new DistanceMove(Distance.feet(16), 0.2),
                        new MiscMovement(0, 0.1)
                )
        );

        Util.log("AUTO INIT");
        Util.writelog(telemetry);
    }

    @Override
    public void loop() {
        motion.update(time);
//        Util.log(String.format("hdg %.2d",sensors.gyroHeading));
        if (sensors.gyro.isCalibrating()) Util.log("WARN: STILL CALIBRATING");
        Util.writelog(telemetry);
    }

    @Override
    public void stop() {
        Util.log("AUTO STOP");
        Util.writelog(telemetry);
    }
}
