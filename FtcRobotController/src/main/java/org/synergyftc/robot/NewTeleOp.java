package org.synergyftc.robot;

import com.qualcomm.robotcore.eventloop.opmode.OpMode;

import org.synergyftc.robot.motion.AddedMovements;
import org.synergyftc.robot.motion.LinearMovement;
import org.synergyftc.robot.motion.MiscMovement;
import org.synergyftc.robot.motion.MotionController;
import org.synergyftc.robot.motion.RotationalMovement;

import java.util.Iterator;

/**
 * Created by Logan on 1/16/2016.
 */
public class NewTeleOp extends OpMode {
    double previousTime;
    Motors motors;
    Sensors sensors;
    MotionController motion;
    LinearMovement translation = new LinearMovement(0);
    RotationalMovement turning = new RotationalMovement(0);
    MiscMovement misc = new MiscMovement();
    int servoLastChosen = 0;
    final int SERVO_LEFT = 1, SERVO_RIGHT = 2;

    @Override
    public void init() {
        Util.log("TELEOP START");
        motors = new Motors(this);
        sensors = new Sensors(this);
        motion = new MotionController(motors, sensors, new AddedMovements(translation, turning, misc));
    }

    @Override
    public void loop() {
        double dt = time - previousTime;

        //control driving
        double throttle = -Util.scaleInput(gamepad1.left_stick_y + gamepad2.left_stick_y);
        double direction = Util.scaleInput(gamepad1.right_stick_x + gamepad2.right_stick_x);
        translation.setSpeed(throttle);
        turning.setSpeed(direction);

        //control misc motors
        misc.setSpinner((gamepad1.right_trigger - gamepad1.left_trigger) + (gamepad2.right_trigger - gamepad2.left_trigger));
        misc.setCollector((gamepad1.left_bumper || gamepad2.left_bumper) ? 1 : (gamepad1.right_bumper || gamepad2.right_bumper) ? -1 : 0);

        //update motor values
        motion.update(time);

        //control the dumper
        if (gamepad1.x || gamepad2.x) motors.dumper.forward();
        else if (gamepad1.b || gamepad2.b) motors.dumper.backward();
        else motors.dumper.stop();

        //control the climber things
        if (servoLastChosen == SERVO_LEFT) {
            motors.climberLeft.setPosition(Motors.SERVO_CLIMBER_EXTEND);
        }
        else if (servoLastChosen == SERVO_RIGHT) {
            motors.climberRight.setPosition(1-Motors.SERVO_CLIMBER_EXTEND);
        }
        if (gamepad1.dpad_left || gamepad2.dpad_left) {
            motors.climberLeft.setPosition(Motors.SERVO_CLIMBER_EXTEND_MAX);
            servoLastChosen = SERVO_LEFT;
        }
        else if (gamepad1.dpad_right || gamepad2.dpad_right) {
            motors.climberRight.setPosition(1-Motors.SERVO_CLIMBER_EXTEND_MAX);
            servoLastChosen = SERVO_RIGHT;
        }
        else if (gamepad1.dpad_down) {
            motors.climberTop.setPosition(Motors.SERVO_TOP_DUMP);
        }
        else if (gamepad1.dpad_up || gamepad2.dpad_up) {
            motors.climberLeft.setPosition(Motors.SERVO_CLIMBER_RETRACT);
            motors.climberRight.setPosition(1 - Motors.SERVO_CLIMBER_RETRACT);
            motors.climberTop.setPosition(Motors.SERVO_TOP_RETRACT);
            servoLastChosen = 0;
        }

        double spinnerPos = ((double)motors.spinner.getCurrentPosition() % motors.SPINNER_STEPS_PER_REV) / motors.SPINNER_STEPS_PER_REV;
        int spm = spinnerPos > 0.0 && spinnerPos < 0.6 ? 0 : 1;
        sensors.indicator.setPulseWidthPeriod(100);
        sensors.indicator.setPulseWidthOutputTime(spm == 1 ? 100 : 0);

//        telemetry.addData("t", time);
//        telemetry.addData("gh", sensors.gyro.rawZ());
//            Util.writelog(telemetry);
          telemetry.addData("senc", motors.spinner.getCurrentPosition());
//        telemetry.addData("dt", dt);
//        telemetry.addData("lsy", gamepad1.left_stick_y);
//        telemetry.addData("rsx", gamepad1.right_stick_y);
//        telemetry.addData("gyx", sensors.gyro.getHeading());

        previousTime = time;
    }
}
