package org.synergyftc.robot;

/**
 * Created by Logan on 1/24/2016.
 */
public class Time {
    public static double ms(double ms) {
        return ms*1000;
    }
    public static double seconds(double s) {
        return s;
    }
    public static double minutes(double min) {
        return min/60;
    }
}
