package org.synergyftc.robot;

import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.hardware.DcMotorController;
import com.qualcomm.robotcore.util.Range;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Logan on 1/16/2016.
 */
public class DCMotorX {
    protected static List<DCMotorX> motors = new ArrayList<DCMotorX>();
    protected int encoderZero = 0;

    /**
     * Updates all motors. Accepts time since last update.
     * @param dt time (seconds) since last update
     */
    public static void updateMotors(double dt) {
        for (DCMotorX motor : motors) {
            motor.update(dt);
        }
    }
    public static double masterAcc = 1.0; //global acceleration multiplier

    public final DcMotor motor;
    private double powerTarget = 0.0;
    private double powerActual = 0.0;
    private double acceleration = 10.0; // unit s^-1

    /**
     * Custom motor controller with power ramping, other control features
     * @param motor DcMotor to control (use hardwareMap to obtain)
     * @param PID whether or not to enable PID
     */
    public DCMotorX(DcMotor motor, boolean PID) {
        this.motor = motor;
        if (PID) this.motor.setMode(DcMotorController.RunMode.RUN_USING_ENCODERS);
        else if (motor != null) this.motor.setMode(DcMotorController.RunMode.RUN_WITHOUT_ENCODERS);
        motors.add(this);
    }

    public DCMotorX(DcMotor motor) {
        this(motor, false);
    }

    /**
     * Called by DCMotorX.updateMotors().
     * Allows power ramping.
     * @param dt change in time (seconds)
     */
    private void update(double dt) {
        double acceleration = this.acceleration*masterAcc*dt;
        if (powerActual < powerTarget - acceleration)
            powerActual += acceleration;
        else if (powerActual > powerTarget + acceleration)
            powerActual -= acceleration;
        else
            powerActual = powerTarget;
        motor.setPower(powerTarget); //// FIXME: 1/27/2016
    }

    /**
     * Set target motor power.
     * Clips automatically
     * @param power
     */
    public void setPower(double power) {
        this.powerTarget = Range.clip(power,-1,1);
    }

    /**
     * Sets target motor power and immediately updates actual power to match.
     * @param power
     */
    public void setPowerNow(double power) {
        setPower(power);
        powerActual = powerTarget;
    }

    /**
     * Returns actual current motor power
     * @return actual power
     */
    public double getPower() {
        return powerActual;
    }

    public void resetEncoder() {
        encoderZero = motor.getCurrentPosition();
    }

    /**
     * Returns current encoder position
     * @return encoder position
     */
    public int getCurrentPosition() {
        return motor.getCurrentPosition() - encoderZero;
    }
}
