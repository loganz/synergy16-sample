package org.synergyftc.robot.motion;

import com.qualcomm.robotcore.util.Range;

import org.synergyftc.robot.DCMotorX;
import org.synergyftc.robot.Motors;
import org.synergyftc.robot.Sensors;

import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;

/**
 * Created by Logan on 1/24/2016.
 */
public class MiscMovement extends Movement {
    private double speedCollector, speedSpinner;

    public MiscMovement() {
        this(0,0);
    }
    public MiscMovement(double collector, double spinner) {
        this.speedCollector = collector;
        this.speedSpinner = spinner;
    }

    public void setCollector(double speed) {speedCollector = speed;}
    public void setSpinner(double speed) {speedSpinner = speed;}

    @Override
    public Map<DCMotorX, Double> calculate(Motors motors, Sensors sensors, double dt) {
        Map<DCMotorX, Double> output = new HashMap<DCMotorX, Double>();
        output.put(motors.spinner, speedSpinner);
        output.put(motors.collector, speedCollector);
        return output;
    }
}
