package org.synergyftc.robot.motion;

import org.synergyftc.robot.DCMotorX;
import org.synergyftc.robot.Motors;
import org.synergyftc.robot.Sensors;

import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;

/**
 * Combines multiple movements for simultaneous motion.
 * Motor power calculations for each motion are performed and summed to create output powers.
 * This movement is completed once all of its Completable movements (if any) are completed.
 * Created by Logan on 1/23/2016.
 */
public class AddedMovements extends Movement implements Completable {
    private Movement[] movements;

    public AddedMovements(Movement... movements) {
        this.movements = movements;
    }

    @Override
    public void start(Motors motors, Sensors sensors) {
        for (Movement movement:movements) {
            movement.start(motors, sensors);
        }
    }

    public Map<DCMotorX, Double> calculate(Motors motors, Sensors sensors, double time) {
        Map<DCMotorX, Double> output = new HashMap<DCMotorX, Double>();
        for (Movement movement : movements) {
            Map<DCMotorX, Double> map = movement.calculate(motors, sensors, time);
            for (DCMotorX motor : map.keySet()) {
                if (!output.containsKey(motor))
                    output.put(motor, 0d);
                output.put(motor, output.get(motor) + map.get(motor));
            }
        }
        return output;
    }

    public boolean isComplete() {
        return ((Completable)movements[0]).isComplete();
    }
}
