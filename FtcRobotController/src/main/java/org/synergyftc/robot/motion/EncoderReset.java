package org.synergyftc.robot.motion;

import com.qualcomm.robotcore.hardware.DcMotorController;

import org.synergyftc.robot.DCMotorX;
import org.synergyftc.robot.Motors;
import org.synergyftc.robot.Sensors;
import org.synergyftc.robot.Util;

import java.util.HashMap;
import java.util.Map;

/**
 * Performs a reset of drive motor encoders when run in a MotionController.
 * Created by Logan on 1/27/2016.
 */
public class EncoderReset extends Movement implements Completable {
    protected boolean complete = false;

    public EncoderReset() {

    }

    @Override
    public void start(Motors motors, Sensors sensors) {
        motors.left.motor.setMode(DcMotorController.RunMode.RESET_ENCODERS);
        motors.right.motor.setMode(DcMotorController.RunMode.RESET_ENCODERS);
        complete = false;
    }

    @Override
    public Map<DCMotorX, Double> calculate(Motors motors, Sensors sensors, double dt) {
        motors.stopAll();
        complete = motors.left.getCurrentPosition() == 0 &&
                motors.right.getCurrentPosition() == 0;
        Util.log("ENCP" + motors.left.getCurrentPosition() + " " + motors.right.getCurrentPosition());
        if (complete) {
            motors.left.motor.setMode(DcMotorController.RunMode.RUN_USING_ENCODERS);
            motors.right.motor.setMode(DcMotorController.RunMode.RUN_USING_ENCODERS);
        }
        Map<DCMotorX, Double> output = new HashMap<DCMotorX, Double>();
        return output;
    }

    @Override
    public boolean isComplete() {
        return complete;
    }
}
