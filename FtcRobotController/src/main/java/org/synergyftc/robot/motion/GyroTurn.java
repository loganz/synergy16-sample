package org.synergyftc.robot.motion;

import org.synergyftc.robot.DCMotorX;
import org.synergyftc.robot.Motors;
import org.synergyftc.robot.Sensors;
import org.synergyftc.robot.Util;

import java.util.Map;

/**
 * Created by Logan on 1/24/2016.
 */
public class GyroTurn extends RotationalMovement implements Completable {
    private double finalAngle,  targetSpeed;
    private boolean complete = false;

    public GyroTurn(double finalAngle, double speed) {
        super(speed);
        this.targetSpeed = speed;
        this.finalAngle = finalAngle;
    }

    @Override
    public void start(Motors motors, Sensors sensors) {
        complete = false;
        Util.log("GYRO START TURN");
    }

    @Override
    public Map<DCMotorX, Double> calculate(Motors motors, Sensors sensors, double dt) {
        double da = Util.signedAngleDiff(finalAngle, sensors.gyroHeading);
        speed = Math.signum(da)*targetSpeed * (Math.abs(da) < 45 ? Math.abs(da) / 45 : 1);
        speed = Math.max(Math.abs(speed), 0.05) * Math.signum(speed);
//        Util.log(String.format("A: %.2f T: %.2f", sensors.gyroHeading, finalAngle));
        if (Math.abs(da) < 4.25) complete = true;

        return super.calculate(motors, sensors, dt);
    }

    @Override
    public boolean isComplete() {
        return complete;
    }
}
