package org.synergyftc.robot.motion;

import org.synergyftc.robot.Motors;
import org.synergyftc.robot.Sensors;
import org.synergyftc.robot.Util;

import java.util.LinkedList;
import java.util.Queue;

/**
 * Created by Logan on 1/24/2016.
 */
public class SequentialMotionController extends MotionController {
    private Queue<Movement> sequence = new LinkedList<Movement>();
    private int ctr = 0;

    public SequentialMotionController(Motors motors, Sensors sensors) {
        super(motors, sensors, null);
    }

    @Override
    public void start(double absoluteTime) {
        super.start(absoluteTime);
        Movement movement = sequence.peek();
        if (movement != null) {
            Util.log("");
            movement.start(motors, sensors);
        }
    }

    public void addMovement(Movement movement) {
        if (movement instanceof Completable)
            sequence.add(movement);
        else
            throw new RuntimeException("Cannot add uncompletable movement.");
    }

    public void addMovement(Movement... movements) {
        for (Movement m : movements)
            addMovement(m);
    }

    public void removeMovement(Movement movement) {
        sequence.remove(movement);
    }

    @Override
    protected void updateMovement(double dt) {
        Movement movement = sequence.peek();
        //safety checks
        if (movement == null || sensors.gyro.isCalibrating()) {
            return;
        }
        //apply any movement that is incomplete
        else if (!(((Completable)movement).isComplete())) {
            movement.apply(motors, sensors, dt);
        }
        else {
            motors.stopAll();
        }
        //advance movement sequence when a movement is completed
        if (((Completable)movement).isComplete()) {
            sequence.poll();
            Movement next = sequence.peek();
            if (next != null) next.start(motors, sensors);
            Util.log("ADV TO "+(ctr++));
            motors.stopAll();
        }
    }
}
