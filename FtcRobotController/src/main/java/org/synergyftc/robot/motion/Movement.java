package org.synergyftc.robot.motion;

import org.synergyftc.robot.DCMotorX;
import org.synergyftc.robot.Motors;
import org.synergyftc.robot.Sensors;

import java.util.Map;

/**
 * Created by Logan on 1/23/2016.
 */
public abstract class Movement {
    public void start(Motors motors, Sensors sensors) {

    }
    public abstract Map<DCMotorX, Double> calculate(Motors motors, Sensors sensors, double dt);
    public void apply(Motors motors, Sensors sensors, double dt) {
        Map<DCMotorX, Double> powers = calculate(motors, sensors, dt);
        for (DCMotorX motor : powers.keySet()) {
            double power = powers.get(motor);
            motor.setPower(power);
        }
    }
}
