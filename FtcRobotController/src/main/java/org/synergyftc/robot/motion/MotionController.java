package org.synergyftc.robot.motion;

import com.qualcomm.robotcore.util.RobotLog;

import org.synergyftc.robot.DCMotorX;
import org.synergyftc.robot.Motors;
import org.synergyftc.robot.Sensors;
import org.synergyftc.robot.Util;

/**
 * Updates and applies a single Movement. AddedMovements allows multiple simultaneous movements to
 * be performed.  For a series of Completable Movements (e.g. Autonomous period), one should use
 * the SequentialMotionController.
 * Created by Logan on 1/23/2016.
 */
public class MotionController {
    public static double MAX_TIME = 60*2.5; //max runtime in sec
    protected final Sensors sensors;
    protected final Motors motors;
    protected boolean running = false;
    protected double startTime, lastTime;
    private Movement movement;

    public MotionController(Motors motors, Sensors sensors, Movement movement) {
        this.motors = motors;
        this.sensors = sensors;
        this.movement = movement;
    }

    /**
     * Starts the motion controller.
     * @param absoluteTime the current robot controller time
     */
    public void start(double absoluteTime) {
        if (sanityCheck()) Util.log("MC START NORMAL");
        else Util.log("WARN SANITY");
//        RobotLog.d(String.format("MotionController started @ %.2f", absoluteTime));
        running = true;
        startTime = absoluteTime;
        lastTime = 0;
    }

    public boolean sanityCheck() {
        boolean sane = true;
        double voltage = sensors.voltage.getVoltage();
        Util.log(String.format("VOLTAGE %.2d", voltage));
        if (voltage < 0.4) {
            Util.log("WARN VOLTAGE");
            sane = false;
        }
        return sane;
    }

    /**
     * Stops the motion controller and halts motors.
     */
    public void stop(double absoluteTime) {
        Util.log("MC STOP");
//        RobotLog.d(String.format("MotionController STOP @ %.2f", absoluteTime));
        running = false;
        motors.stopAll();
    }

    /**
     * Gets the time since the motion controller was started.
     * @return time since start in seconds
     */
    public double getMotionTime(double absoluteTime) {
        return absoluteTime - startTime;
    }

    /**
     * Updates the motion controller and writes new motor outputs. Also calls update method of
     * the Sensors instance.
     * @param absoluteTime the current robot controller time
     */
    public void update(double absoluteTime) {
//        if (!running) return;
        Util.rcTime = absoluteTime;
        double time = getMotionTime(absoluteTime);
//        if (time >= MAX_TIME) {
//            Util.log("MC TIMEOUT");
////            RobotLog.d(String.format("MC TIMEOUT @ "+absoluteTime));
//            stop(absoluteTime);
//            return;
//        }
        double dt = time - lastTime;
        sensors.update(dt);
        updateMovement(dt);
        DCMotorX.updateMotors(dt);
        lastTime = time;
    }

    protected void updateMovement(double dt) {
        this.movement.apply(motors, sensors, dt);
    }
}
