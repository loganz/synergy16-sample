package org.synergyftc.robot.motion;

import com.qualcomm.robotcore.util.Range;

import org.synergyftc.robot.DCMotorX;
import org.synergyftc.robot.Motors;
import org.synergyftc.robot.Sensors;

import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;

/**
 * Created by Logan on 1/24/2016.
 */
public class RotationalMovement extends Movement {
    protected double speed;
    public RotationalMovement(double speed) {
        this.speed = speed;
    }

    public void setSpeed(double speed) {
        this.speed = Range.clip(speed, -1, 1);
    }
    public double getSpeed() {
        return speed;
    }

    @Override
    public Map<DCMotorX, Double> calculate(Motors motors, Sensors sensors, double dt) {
        Map<DCMotorX, Double> output = new HashMap<DCMotorX, Double>();
        output.put(motors.left, speed);
        output.put(motors.right, -speed);
        return output;
    }
}
