package org.synergyftc.robot.motion;

import org.synergyftc.robot.DCMotorX;
import org.synergyftc.robot.Motors;
import org.synergyftc.robot.Sensors;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Logan on 1/27/2016.
 */
public class Pause extends Movement implements Completable {
    protected double time, length;
    protected boolean complete = false;

    public Pause(double time) {
        this.length = time;
    }

    @Override
    public void start(Motors motors, Sensors sensors) {
        time = 0;
        complete = false;
    }

    @Override
    public Map<DCMotorX, Double> calculate(Motors motors, Sensors sensors, double dt) {
        motors.stopAll();
        time += dt;
        if (time >= length)
            complete = true;
        Map<DCMotorX, Double> output = new HashMap<DCMotorX, Double>();
        return output;
    }

    @Override
    public boolean isComplete() {
        return complete;
    }
}
