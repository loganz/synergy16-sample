package org.synergyftc.robot.motion;

import com.qualcomm.robotcore.util.Range;

import org.synergyftc.robot.DCMotorX;
import org.synergyftc.robot.Motors;
import org.synergyftc.robot.Sensors;
import org.synergyftc.robot.Util;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Logan on 1/24/2016.
 */
public class CDistanceMove extends LinearMovement implements Completable {
    private static final int ENC_TOLERANCE = 50;
    private boolean complete = false, started = false;
    private int encSteps;
    private int targetPos;
    private double targetAngle;

    public CDistanceMove(int encSteps, double speed) {
        super(speed);
        this.encSteps = encSteps;
    }

    public int getPos(Motors motors) {
        return (motors.left.getCurrentPosition() + motors.right.getCurrentPosition()) / 2;
    }

    @Override
    public void start(Motors motors, Sensors sensors) {
        complete = false;
        Util.log("ESTEP "+encSteps);
        int pos = getPos(motors);
        targetPos = pos + encSteps;
        targetAngle = sensors.gyroHeading;
        speed = Math.abs(speed) * Math.signum(encSteps);
    }

    @Override
    public Map<DCMotorX, Double> calculate(Motors motors, Sensors sensors, double dt) {
        int pos = getPos(motors);
        if (Math.abs(pos - targetPos) < CDistanceMove.ENC_TOLERANCE)
            complete = true;
        double correct = Range.clip(Util.signedAngleDiff(targetAngle, sensors.gyroHeading) / 20, -1, 1);
        Map<DCMotorX, Double> output = new HashMap<DCMotorX, Double>();
        output.put(motors.left, speed + correct);
        output.put(motors.right, speed - correct);
        return output;
    }

    @Override
    public boolean isComplete() {
        return complete;
    }
}
