package org.synergyftc.robot.motion;

import org.synergyftc.robot.Motors;
import org.synergyftc.robot.Sensors;

/**
 * Created by Logan on 1/24/2016.
 */
public interface Completable {
    boolean isComplete();
}
