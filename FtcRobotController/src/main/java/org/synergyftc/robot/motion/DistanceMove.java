package org.synergyftc.robot.motion;

import com.qualcomm.robotcore.util.Range;

import org.synergyftc.robot.DCMotorX;
import org.synergyftc.robot.Motors;
import org.synergyftc.robot.Sensors;
import org.synergyftc.robot.Util;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Logan on 1/24/2016.
 */
public class DistanceMove extends LinearMovement implements Completable {
    private static final int ENC_TOLERANCE = 50;
    private boolean complete = false, started = false;
    private int encSteps;
    private int targetPos;

    public DistanceMove(int encSteps, double speed) {
        super(speed);
        this.encSteps = encSteps;
    }

    public int getPos(Motors motors) {
        return (motors.left.getCurrentPosition() + motors.right.getCurrentPosition()) / 2;
    }

    @Override
    public void start(Motors motors, Sensors sensors) {
        complete = false;
        Util.log("ESTEP "+encSteps);
        int pos = getPos(motors);
        targetPos = pos + encSteps;
        Util.log("ETRGT "+targetPos);
        speed = Math.abs(speed) * Math.signum(encSteps);
    }

    @Override
    public Map<DCMotorX, Double> calculate(Motors motors, Sensors sensors, double dt) {
        int pos = getPos(motors);
        if (Math.abs(pos - targetPos) < DistanceMove.ENC_TOLERANCE)
            complete = true;
        return super.calculate(motors, sensors, dt);
    }

    @Override
    public boolean isComplete() {
        return complete;
    }
}
