package org.synergyftc.robot.motion;

import org.synergyftc.robot.DCMotorX;
import org.synergyftc.robot.Motors;
import org.synergyftc.robot.Sensors;
import org.synergyftc.robot.Time;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Logan on 1/27/2016.
 */
public class ClimberDumpMovement extends Movement implements Completable {
    protected double time, length;
    protected boolean staged = false;
    protected boolean complete = false;

    public ClimberDumpMovement() {
        this.length = Time.seconds(4.0);
    }

    @Override
    public void start(Motors motors, Sensors sensors) {
        time = 0;
        motors.climberTop.setPosition(Motors.SERVO_TOP_DUMP);
        complete = false;
    }

    @Override
    public Map<DCMotorX, Double> calculate(Motors motors, Sensors sensors, double dt) {
        motors.stopAll();
        time += dt;
        if (time >= length/2 && !staged) {
            staged = true;
            motors.climberTop.setPosition(Motors.SERVO_TOP_RETRACT);
        }
        if (time >= length)
            complete = true;
        Map<DCMotorX, Double> output = new HashMap<DCMotorX, Double>();
        return output;
    }

    @Override
    public boolean isComplete() {
        return complete;
    }
}
