package org.synergyftc.robot;

import com.qualcomm.robotcore.robocol.Telemetry;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

/**
 * Created by Logan on 1/16/2016.
 */
public class Util {
    public static Queue<String> logdata = new LinkedList<String>();
    public static double rcTime = 0;

    public static double scaleInput(double input) {
        return input*input*Math.signum(input);
//        double[] weights = {0.0, 0.1, 0.15, 0.2, 0.3, 0.5, 0.7, 1.0};
//        double left = weights[(int)(Math.floor(Math.abs(input) / weights.length))];
//        double right = weights[(int)Math.min(weights.length-1, Math.ceil(Math.abs(input) / weights.length))];
//        double out = left + (right-left) * (input-Math.floor(input/weights.length));
//        return Math.signum(input)*out;
        }
    public static double signedAngleDiff(double deg1, double deg2) {
        double diff = deg2rad(deg1) - deg2rad(deg2);
        double angle = rad2deg(Math.atan2(Math.sin(diff), Math.cos(diff))); //calculate signed angle difference
        return angle;
    }
    public static double deg2rad(double deg) {
        return deg*(Math.PI/180d);
    }
    public static double rad2deg(double rad) {
        return rad*(180d/Math.PI);
    }
    public static void log(String s) {
        if (logdata.size() >= 4)
            logdata.poll();
        logdata.add(String.format("%.2f: %s", rcTime, s));
    }
    public static void writelog(Telemetry telemetry) {
        int i=0;
        Iterator it = Util.logdata.iterator();
        while (it.hasNext()) {
            telemetry.addData("l"+(i++), it.next());
        }
    }
}
