package org.synergyftc.robot;

import com.qualcomm.hardware.ModernRoboticsI2cGyro;
import com.qualcomm.robotcore.eventloop.opmode.OpMode;
import com.qualcomm.robotcore.hardware.DigitalChannel;
import com.qualcomm.robotcore.hardware.DigitalChannelController;
import com.qualcomm.robotcore.hardware.GyroSensor;
import com.qualcomm.robotcore.hardware.PWMOutput;
import com.qualcomm.robotcore.hardware.VoltageSensor;

/**
 * Created by Logan on 1/23/2016.
 */
public class Sensors {
    public ModernRoboticsI2cGyro gyro;
    public VoltageSensor voltage;
    public PWMOutput indicator;
    public double gyroHeading = 0;

    public Sensors(OpMode opmode) {
        gyro = (ModernRoboticsI2cGyro)opmode.hardwareMap.gyroSensor.get("gyro");
        gyro.resetZAxisIntegrator();
        gyro.calibrate();
        gyroHeading = 0;

        indicator = opmode.hardwareMap.pwmOutput.get("indicator");
        voltage = opmode.hardwareMap.voltageSensor.get("Motor Controller 1");
    }
    public void update(double dt) {
        gyroHeading = gyro.getHeading();
    }
}
