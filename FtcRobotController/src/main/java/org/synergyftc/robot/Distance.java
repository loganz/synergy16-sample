package org.synergyftc.robot;

/**
 * Created by Logan on 1/27/2016.
 */
public class Distance {
    public static final double CORRECT = 0.6372;

    public static int inches(double inches) {
        return (int)(CORRECT * Motors.STEPS_PER_REVOLUTION * inches / (Math.PI * Motors.WHEEL_DIAMETER_IN));
    }
    public static int feet(double feet) {
        return inches(feet*12);
    }
}
