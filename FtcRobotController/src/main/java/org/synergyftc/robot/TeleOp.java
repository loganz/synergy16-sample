package org.synergyftc.robot;

import com.qualcomm.robotcore.eventloop.opmode.OpMode;
import com.qualcomm.robotcore.hardware.GyroSensor;

/**
 * Created by Logan on 1/16/2016.
 */
public class TeleOp extends OpMode {
    double previousTime;
    Motors motors;
    Sensors sensors;

    @Override
    public void init() {
        motors = new Motors(this);
        sensors = new Sensors(this);
    }

    @Override
    public void loop() {
        double dt = time - previousTime;

        //control driving
        double throttle = -gamepad1.left_stick_y;
        double direction = gamepad1.right_stick_x;
        double right = Util.scaleInput(throttle - direction);
        double left = Util.scaleInput(throttle + direction);
        motors.right.setPower(-right);
        motors.left.setPower(left);

        //control misc motors
        motors.spinner.setPowerNow(gamepad1.right_trigger - gamepad1.left_trigger);
        motors.collector.setPowerNow(gamepad1.left_bumper ? 1 : gamepad1.right_bumper ? -1 : 0);

        //update motor values
        DCMotorX.updateMotors(dt);

        //control the dumper
        if (gamepad1.x) motors.dumper.forward();
        else if (gamepad1.b) motors.dumper.backward();
        else motors.dumper.stop();

        telemetry.addData("time", time);
        telemetry.addData("dt", dt);
        telemetry.addData("lsy", gamepad1.left_stick_y);
        telemetry.addData("gyx", sensors.gyro.getHeading());

        previousTime = time;
    }
}
