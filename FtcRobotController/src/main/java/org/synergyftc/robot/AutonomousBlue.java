package org.synergyftc.robot;

import com.qualcomm.robotcore.eventloop.opmode.OpMode;

import org.synergyftc.robot.motion.AddedMovements;
import org.synergyftc.robot.motion.CDistanceMove;
import org.synergyftc.robot.motion.DistanceMove;
import org.synergyftc.robot.motion.GyroTurn;
import org.synergyftc.robot.motion.MiscMovement;
import org.synergyftc.robot.motion.Pause;
import org.synergyftc.robot.motion.SequentialMotionController;

/**
 * Created by Logan on 1/24/2016.
 */
public class AutonomousBlue extends OpMode {
    Motors motors;
    Sensors sensors;
    SequentialMotionController motion;

    @Override
    public void init() {
        motors = new Motors(this);
        sensors = new Sensors(this);
        motion = new SequentialMotionController(motors, sensors);
        motors.left.resetEncoder();
        motors.right.resetEncoder();

        motion.addMovement(
                new CDistanceMove(Distance.inches(0.1), 0.5), //// FIXME: 1/28/2016
                new AddedMovements(
                        new DistanceMove(Distance.feet(-0.2), 0.5),
                        new MiscMovement(-1, 0)
                ),
                new Pause(0.3),
                new GyroTurn(45, 0.4),
                new Pause(0.3),
                new AddedMovements(
                        new DistanceMove(Distance.feet(-4.73), 0.3),
                        new MiscMovement(-1, 0)
                ),
                new Pause(0.3),
                new GyroTurn(360-45, 0.4),
                new DistanceMove(Distance.feet(2.5), 0.2),
                new DistanceMove(Distance.feet(-1.0), 0.2),
                new DistanceMove(Distance.feet(5), 1.0),
                new AddedMovements(
                        new DistanceMove(Distance.feet(15), 1.0),
                        new MiscMovement(0, 0.4)
                )
        );

        Util.log("AUTO INIT");
        Util.writelog(telemetry);
    }

    @Override
    public void loop() {
        motion.update(time);
//        Util.log(String.format("hdg %.2d",sensors.gyroHeading));
        Util.writelog(telemetry);
    }

    @Override
    public void stop() {
        Util.log("AUTO STOP");
        Util.writelog(telemetry);
    }
}
