package org.synergyftc.robot;

import com.qualcomm.robotcore.hardware.Servo;

/**
 * Created by Logan on 1/16/2016.
 */
public class CRServoX {
    public enum CRMode {
        STOP (0.493),
        FORWARD (1.0),
        BACKWARD (0.0);

        private final double position;
        CRMode(double position) {
            this.position = position;
        }
    }

    public Servo servo;
    public CRServoX(Servo servo) {
        this.servo = servo;
    }

    public void stop() {
        servo.setPosition(CRMode.STOP.position);
    }
    public void forward() {
        servo.setPosition(CRMode.FORWARD.position);
    }
    public void backward() {
        servo.setPosition(CRMode.BACKWARD.position);
    }
}
