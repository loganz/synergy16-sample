package org.synergyftc.robot;

import com.qualcomm.robotcore.eventloop.opmode.OpMode;
import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.hardware.DcMotorController;
import com.qualcomm.robotcore.hardware.Servo;

/**
 * Created by Logan on 1/23/2016.
 */
public class Motors {
    public Servo climberLeft, climberRight, climberTop;
    public DCMotorX left, right, spinner, collector;
    public CRServoX dumper;
    public static final double SERVO_CLIMBER_EXTEND = 0.4;
    public static final double SERVO_CLIMBER_EXTEND_MAX = 0.5;
    public static final double SERVO_CLIMBER_RETRACT = 0.12;
    public static final double SERVO_TOP_DUMP = 0.1;
    public static final double SERVO_TOP_RETRACT = 0.98;
    public static final int STEPS_PER_REVOLUTION = 1120; //varies based on gearbox
    public static final int SPINNER_STEPS_PER_REV = 3173; //via calibration
    public static final double WHEEL_DIAMETER_IN = 2.0;

    public Motors(OpMode opmode) {
        left = new DCMotorX(opmode.hardwareMap.dcMotor.get("mLeft"), true);
        right = new DCMotorX(opmode.hardwareMap.dcMotor.get("mRight"), true);
        right.motor.setDirection(DcMotor.Direction.REVERSE);
        collector = new DCMotorX(opmode.hardwareMap.dcMotor.get("mCollector"));
        spinner = new DCMotorX(opmode.hardwareMap.dcMotor.get("mSpinner"), true);
        dumper = new CRServoX(opmode.hardwareMap.servo.get("crDumper"));
        climberLeft = opmode.hardwareMap.servo.get("sClimberL");
        climberRight = opmode.hardwareMap.servo.get("sClimberR");
        climberTop = opmode.hardwareMap.servo.get("sClimberT");
        climberRight.setPosition(1-SERVO_CLIMBER_RETRACT);
        climberLeft.setPosition(SERVO_CLIMBER_RETRACT);
        climberTop.setPosition(SERVO_TOP_RETRACT);
        dumper.stop();

        left.resetEncoder();
        right.resetEncoder();
        spinner.resetEncoder();
    }

    /**
     * Stops all motion associated with these robot motors.
     */
    public void stopAll() {
        left.setPowerNow(0);
        right.setPowerNow(0);
        collector.setPowerNow(0);
        spinner.setPowerNow(0);
        dumper.stop();
    }
}
